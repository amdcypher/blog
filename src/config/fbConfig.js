 import firebase from 'firebase/app'
 import 'firebase/firestore'
 import 'firebase/auth'
 import 'firebase/analytics'
 
 // Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyCV20sJM0DyRym_1mAA8FhxhFeaSoZAvFs",
    authDomain: "anurag-deep.firebaseapp.com",
    databaseURL: "https://anurag-deep.firebaseio.com",
    projectId: "anurag-deep",
    storageBucket: "anurag-deep.appspot.com",
    messagingSenderId: "725289300346",
    appId: "1:725289300346:web:1f6e1480c551c56881e6c3",
    measurementId: "G-C25BQNVK8G"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  firebase.firestore().settings({ timestampsInSnapshots: true })

  export default firebase;